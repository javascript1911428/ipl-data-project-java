import com.ram.ipl.model.Delivery;
import com.ram.ipl.model.Match;

import java.io.BufferedReader;
import java.io.FileReader;
import java.util.*;

public class Main {
    public static final int ID = 0;
    public static final int SEASON = 1;
    public static final int CITY = 2;
    public static final int DATE = 3;
    public static final int TEAM1 = 4;
    public static final int TEAM2 = 5;
    public static final int TOSS_WINNER = 6;
    public static final int TOSS_DECISION = 7;
    public static final int RESULT = 8;
    public static final int DL_APPLIED = 9;
    public static final int WINNER = 10;
    public static final int WIN_BY_RUNS = 11;
    public static final int WIN_BY_WICKETS = 12;
    public static final int PLAYER_OF_MATCH = 13;
    public static final int VENUE = 14;
    public static final int UMPIRE1 = 15;
    public static final int UMPIRE2 = 16;
    public static final int UMPIRE3 = 17;

    public static final int MATCH_ID = 0;
    public static final int INNING = 1;
    public static final int BATTING_TEAM = 2;
    public static final int BOWLING_TEAM = 3;
    public static final int OVER = 4;
    public static final int BALL = 5;
    public static final int BATSMAN = 6;
    public static final int NON_STRIKER = 7;
    public static final int BOWLER = 8;
    public static final int IS_SUPER_OVER = 9;
    public static final int WIDE_RUNS = 10;
    public static final int BYE_RUNS = 11;
    public static final int LEGBYE_RUNS = 12;
    public static final int NOBALL_RUNS = 13;
    public static final int PENALTY_RUNS = 14;
    public static final int BATSMAN_RUNS = 15;
    public static final int EXTRA_RUNS = 16;
    public static final int TOTAL_RUNS = 17;
    public static final int PLAYER_DISMISSED = 18;
    public static final int DISMISSAL_KIND = 19;
    public static final int FIELDER = 20;


    public static List<Match> matches() {
        List<Match> matches = new ArrayList<>();

        try {
            String filepath = "src/com/ram/ipl/data/matches.csv";
            BufferedReader reader = new BufferedReader(new FileReader(filepath));
            String line;
            reader.readLine();
            while ((line = reader.readLine()) != null) {
                String[] fields = line.split(",");

                Match match = new Match();
                //System.out.println(fields[ID] +" "+fields[SEASON]);

                match.setId(fields[ID]);
                match.setSeason(fields[SEASON]);
                match.setCity(fields[CITY]);
                match.setDate(fields[DATE]);
                match.setTeam1(fields[TEAM1]);
                match.setTeam2(fields[TEAM2]);
                match.setTossWinner(fields[TOSS_WINNER]);
                match.setTossDecision(fields[TOSS_DECISION]);
                match.setResult(fields[RESULT]);
                match.setDlApplied(fields[DL_APPLIED]);
                match.setWinner(fields[WINNER]);
                match.setWinByRuns(fields[WIN_BY_RUNS]);
                match.setWinByWickets(fields[WIN_BY_WICKETS]);
                match.setPlayerOfMatch(fields[PLAYER_OF_MATCH]);
                match.setVenue(fields[VENUE]);
                if (fields.length > 15) {
                    match.setUmpire1(fields[UMPIRE1]);
                    match.setUmpire2(fields[UMPIRE2]);
                }
                if (fields.length > 17) {
                    match.setUmpire3(fields[UMPIRE3]);
                }
                matches.add(match);

                //System.out.println(matches.size());
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return matches;
    }

    public static List<Delivery> deliveries() {
        List<Delivery> deliveries = new ArrayList<>();
        try {
            String filepath = "src/com/ram/ipl/data/deliveries.csv";
            BufferedReader reader = new BufferedReader(new FileReader(filepath));
            String line;
            reader.readLine();
            while ((line = reader.readLine()) != null) {
                String[] fields = line.split(",");
                Delivery delivery = new Delivery();

                delivery.setMatchId(fields[MATCH_ID]);
                delivery.setInning(fields[INNING]);
                delivery.setBattingTeam(fields[BATTING_TEAM]);
                delivery.setBowlingTeam(fields[BOWLING_TEAM]);
                delivery.setOver(fields[OVER]);
                delivery.setBall(fields[BALL]);
                delivery.setBatsman(fields[BATSMAN]);
                delivery.setBatsman(fields[NON_STRIKER]);
                delivery.setBowler(fields[BOWLER]);
                delivery.setIsSuperOver(fields[IS_SUPER_OVER]);
                delivery.setWideRuns(fields[WIDE_RUNS]);
                delivery.setByeRuns(fields[BYE_RUNS]);
                delivery.setLegbyeRuns(fields[LEGBYE_RUNS]);
                delivery.setNoballRuns(fields[NOBALL_RUNS]);
                delivery.setPenaltyRuns(fields[PENALTY_RUNS]);
                delivery.setBatsmanRuns(fields[BATSMAN_RUNS]);
                delivery.setExtraRuns(fields[EXTRA_RUNS]);
                delivery.setTotalRuns(fields[TOTAL_RUNS]);
                if (fields.length > 18) {
                    delivery.setPlayerDismissed(fields[PLAYER_DISMISSED]);
                    delivery.setDismissalKind(fields[DISMISSAL_KIND]);
                }
                if (fields.length > 20) {
                    delivery.setFielder(fields[FIELDER]);
                }
                deliveries.add(delivery);

            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return deliveries;
    }

    public static void main(String[] args) {
        List<Match> matches = matches();
        List<Delivery> deliveries = deliveries();

        //System.out.println(matches.get(0).getSeason());


        NumberOfMatchesPlayedPerYear(matches);
        NumberOfMatchesWonOfAllTeams(matches);
        ExtraRunsConcededPerTeamIn2016(matches, deliveries);
        TopTenEconomicalBowlerIn2015(matches, deliveries);
        NoOfTimesBothTossAndMatchWon(matches);
    }

    private static void NumberOfMatchesPlayedPerYear(List<Match> matches) {
        Map<String, Integer> matchesPerYear = new HashMap<>();

        for (Match match : matches) {
            String season = match.getSeason();

            if (season != null && !season.isEmpty()) {
                matchesPerYear.put(season, matchesPerYear.getOrDefault(season, 0) + 1);
            }
        }

        for (Map.Entry<String, Integer> entry : matchesPerYear.entrySet()) {
            System.out.println("Season: " + entry.getKey() + ", Matches Played: " + entry.getValue());
        }
    }

    private static void NumberOfMatchesWonOfAllTeams(List<Match> matches) {
        Map<String, Integer> matchesWonOfAllTeams = new HashMap<>();

        for (Match match : matches) {
            String winner = match.getWinner();

            if (winner != null && !winner.isEmpty()) {
                matchesWonOfAllTeams.put(winner, matchesWonOfAllTeams.getOrDefault(winner, 0) + 1);
            }
        }

        for (Map.Entry<String, Integer> entry : matchesWonOfAllTeams.entrySet()) {
            System.out.println("Team: " + entry.getKey() + ", Matches Won All Over: " + entry.getValue());
        }
    }

    private static void ExtraRunsConcededPerTeamIn2016(List<Match> matches, List<Delivery> deliveries) {
        ArrayList<String> matchIdList = new ArrayList<>();
        for (Match match : matches) {
            String matchId = match.getId();
            if (Objects.equals(match.getSeason(), "2016")) {
                matchIdList.add(matchId);
            }
        }
        String lowestMatchId = matchIdList.get(0);

        String highestMatchId = matchIdList.get(matchIdList.size() - 1);


        Map<String, Integer> extraRunsConcededPerTeam = new HashMap<>();

        for (Delivery delivery : deliveries) {
            if (Integer.parseInt(delivery.getMatchId()) >= Integer.parseInt(lowestMatchId) && Integer.parseInt(delivery.getMatchId()) <= Integer.parseInt(highestMatchId)) {
                String bowlingTeam = delivery.getBowlingTeam();
                int extraRuns = Integer.parseInt(delivery.getExtraRuns());
                extraRunsConcededPerTeam.put(bowlingTeam, extraRunsConcededPerTeam.getOrDefault(bowlingTeam, 0) + extraRuns);
            }
        }

        for (Map.Entry<String, Integer> entry : extraRunsConcededPerTeam.entrySet()) {
            System.out.println("Team: " + entry.getKey() + ", Extra Runs Conceded: " + entry.getValue());
        }
    }


    private static void TopTenEconomicalBowlerIn2015(List<Match> matches, List<Delivery> deliveries) {
        ArrayList<String> matchIdList = new ArrayList<>();
        for (Match match : matches) {
            String matchId = match.getId();
            if (Objects.equals(match.getSeason(), "2015")) {
                matchIdList.add(matchId);
            }
        }
        //System.out.println(matchIdList);
        String lowestMatchId = matchIdList.get(0);

        String highestMatchId = matchIdList.get(matchIdList.size() - 1);


        Map<String, Integer> bowlersData = new HashMap<>();
        Map<String, Integer> bowlerBalls = new HashMap<>();

        for (Delivery delivery : deliveries) {
            int matchId = Integer.parseInt(delivery.getMatchId());


            if (matchId >= Integer.parseInt(lowestMatchId) && matchId <= Integer.parseInt(highestMatchId)) {
                String bowler = delivery.getBowler();
                int runs = Integer.parseInt(delivery.getTotalRuns());
                int legbyeRuns = Integer.parseInt(delivery.getLegbyeRuns());
                int penaltyRuns = Integer.parseInt(delivery.getPenaltyRuns());
                int byeruns = Integer.parseInt(delivery.getByeRuns());

                bowlersData.put(bowler, bowlersData.getOrDefault(bowler, 0) + (runs - (legbyeRuns + penaltyRuns + byeruns)));
                if (delivery.getWideRuns().equals("0") && delivery.getNoballRuns().equals("0")) {
                    bowlerBalls.put(bowler, bowlerBalls.getOrDefault(bowler, 0) + 1);
                }
            }
        }


        Map<String, Double> bowlerEconomy = new HashMap<>();

        for (Map.Entry<String, Integer> entry : bowlersData.entrySet()) {
            //System.out.println(entry.getKey());
            String bowler = entry.getKey();
            Integer balls = bowlerBalls.get(bowler);
            Double overs = (double) (balls / 6);

            Double economy = entry.getValue() / overs;
            String roundedEconomy = String.format("%.2f", economy);

            bowlerEconomy.put(bowler, Double.parseDouble(roundedEconomy));
        }

        List<Map.Entry<String, Double>> resultList = new ArrayList<>(bowlerEconomy.entrySet());

        resultList.sort((a, b) -> a.getValue().compareTo(b.getValue()));

        System.out.println(resultList);


    }

    private static void NoOfTimesBothTossAndMatchWon(List<Match> matches) {
        Map<String, Integer> noOfTimesBothTossAndMatchWon = new HashMap<>();

        for (Match match : matches) {
            String tossWinner = match.getTossWinner();
            System.out.println(tossWinner);
            String winner = match.getWinner();
            System.out.println(winner);
            if (tossWinner.equals(winner)) {

                noOfTimesBothTossAndMatchWon.put(winner, noOfTimesBothTossAndMatchWon.getOrDefault(winner, 0) + 1);
            }
        }
        for (Map.Entry<String, Integer> entry : noOfTimesBothTossAndMatchWon.entrySet()) {
            System.out.println("Winner: " + entry.getKey() + ", No Of Times: " + entry.getValue());
        }

    }
}